﻿using System.Collections.Generic;
using System.Linq;

namespace GameStudioAssignment
{
    public class DictionaryAndListAssignment
    {
        public Dictionary<string, int> statDictionaryDeclare()
        {
            var statDictionary = new Dictionary<string, int>
            {
                {"Strength", 0},
                {"Dexterity", 0},
                {"Constitution", 0},
                {"Intelligence", 0},
                {"Wisdom", 0},
                {"Charisma", 0}
            };
            return statDictionary;
        }

        public List<string> RaceListDeclare()
        {
            var raceList = new List<string>
            {
                "Dwarf",
                "Elf",
                "Halfling",
                "Human"
            };

            return raceList;
        }

        public List<string> SubRaceListDeclare(string race)
        {
            if (race.Equals("Dwarf"))
            {
                var subRaceList = new List<string>
                {
                    "Hill",
                    "Mountain"
                };
                return subRaceList;
            }

            if (race.Equals("Elf"))
            {
                var subRaceList = new List<string>
                {
                    "High",
                    "Wood"
                };
                return subRaceList;
            }

            if (race.Equals("Halfling"))
            {
                var subRaceList = new List<string>
                {
                    "Lightfoot",
                    "Stout"
                };
                return subRaceList;
            }

            if (!race.Equals("Human")) return null;
            {
                var subRaceList = new List<string>
                {
                    "Calishite",
                    "Chondathan",
                    "Damaran",
                    "Illuskan",
                    "Mulan",
                    "Rashemi",
                    "Shou",
                    "Tethyrian",
                    "Turami"
                };
                return subRaceList;
            }
        }

        public List<string> NameListDeclare(string race, string sex, string subRace)
        {
            List<string> nameList;
            switch (race)
            {
                case "Dwarf":

                    switch (sex)
                    {
                        case "Male":
                            var dwarfMaleNames =
                                "Adrik, Alberich, Baern, Barendd, Brottor, Bruenor, Dain, Darrak, Delg, Eberk, Einkil, Fargrim, Flint, Gardain, Harbek, Kildrak, Morgran, Orsik, Oskar, Rangrim, Rurik, Taklinn, Thoradin, Thorin, Tordek, Traubon, Travok, Ulfgar, Veit, Vondal";
                            nameList = dwarfMaleNames.Replace(" ", "").Split(',').ToList();

                            return nameList;

                        case "Female":
                            var dwarfFemaleNames =
                                "Amber, Artin, Audhild, Bardryn, Dagnal, Diesa, Eldeth, Falkrunn, Finellen, Gunnloda, Gurdis, Helja, Hlin, Kathra, Kristryd, Ilde, Liftrasa, Mardred, Riswynn, Sannl, Torbera, Torgga, Vistra";
                            nameList = dwarfFemaleNames.Replace(" ", "").Split(',').ToList();

                            return nameList;
                    }
                    break;

                case "Elf":
                    switch (sex)
                    {
                        case "Male":
                            var elfMaleNames =
                                "Adran, Aelar, Aramil, Arannis, Aust, Beiro, Berrian, Carric, Enialis, Erdan, Erevan, Galinndan, Hadarai, Heian, Himo, Immeral, Ivellios, Laucian, Mindartis, Paelias, Peren, Quarion, Riardon, Rolen, Soveliss, Thamior, Tharivol, Theren, Varis";

                            nameList = elfMaleNames.Replace(" ", "").Split(',').ToList();

                            return nameList;
                        case "Female":
                            var elfFemaleNames =
                                "Adrie, Althaea, Anastrianna, Andraste, Antinua, Bethrynna, Birel, Caelynn, Drusilia, Enna, Felosial, Ielenia, Jelenneth, Keyleth, Leshanna, Lia, Meriele, Mialee, Naivara, Quelenna, Quillathe, Sariel, Shanairra, Shava, Silaqui, Theirastra, Thia, Vadania, Valanthe, Xanaphia";

                            nameList = elfFemaleNames.Replace(" ", "").Split(',').ToList();

                            return nameList;
                    }
                    break;

                case "Halfling":
                    switch (sex)
                    {
                        case "Male":
                            var halflingMaleNames =
                                "Alton, Ander, Cade, Corrin, Eldon, Errich, Finnan, Garret, Lindal, Lyle, Merric, Milo, Osborn, Perrin, Reed, Roscoe, Wellby";

                            nameList = halflingMaleNames.Replace(" ", "").Split(',').ToList();

                            return nameList;
                        case "Female":
                            var halflingFemaleNames =
                                "Andry, Bree, Callie, Cora, Euphemia, Jillian, Kithri, Lavinia, Lidda, Merla, Nedda, Paela, Portia, Seraphina, Shaena, Trym, Vani, Verna";

                            nameList = halflingFemaleNames.Replace(" ", "").Split(',').ToList();

                            return nameList;
                    }
                    break;

                case "Human":
                    switch (subRace)
                    {
                        case "Calishite":
                            switch (sex)
                            {
                                case "Male":
                                    var humanCalishiteMaleNames =
                                        "Aseir, Bardeid, Haseid, Khemed, Mehmen, Sudeiman, Zasheir";

                                    nameList = humanCalishiteMaleNames.Replace(" ", "").Split(',').ToList();

                                    return nameList;
                                case "Female":
                                    var humanCalishiteFemaleNames =
                                        "Atala, Ceidil, Hama, Jasmal, Meilil, Seipora, Yasheira, Zasheida";

                                    nameList = humanCalishiteFemaleNames.Replace(" ", "").Split(',').ToList();

                                    return nameList;
                            }
                            break;

                        case "Chondathan":
                            switch (sex)
                            {
                                case "Male":
                                    var humanChondathanMaleNames =
                                        "Darvin, Dorn, Evendur, Gorstag, Grim, Helm, Malark, Morn, Randal, Stedd";

                                    nameList = humanChondathanMaleNames.Replace(" ", "").Split(',').ToList();

                                    return nameList;
                                case "Female":
                                    var humanChondathanFemaleNames =
                                        "Arveene, Esvele, Jhessail, Kerri, Lureene, Miri, Rowan, Shandri, Tessele";

                                    nameList = humanChondathanFemaleNames.Replace(" ", "").Split(',').ToList();

                                    return nameList;
                            }
                            break;

                        case "Damaran":
                            switch (sex)
                            {
                                case "Male":
                                    var humanDamaranMaleNames =
                                        "Bor, Fodel, Glar, Grigor, Igan, Ivor, Kosef, Mival, Orel, Pavel, Sergor";

                                    nameList = humanDamaranMaleNames.Replace(" ", "").Split(',').ToList();

                                    return nameList;
                                case "Female":
                                    var humanDamaranFemaleNames =
                                        "Alethra, Kara, Katernin, Mara, Natali, Olma, Tana, Zora";
                                    nameList = humanDamaranFemaleNames.Replace(" ", "").Split(',').ToList();

                                    return nameList;
                            }
                            break;

                        case "Illuskan":
                            switch (sex)
                            {
                                case "Male":
                                    var humanIlluskanMaleNames =
                                        "Ander, Blath, Bran, Frath, Geth, Lander, Luth, Malcer, Stor, Taman, Urth";

                                    nameList = humanIlluskanMaleNames.Replace(" ", "").Split(',').ToList();

                                    return nameList;
                                case "Female":
                                    var humanIlluskanFemaleNames =
                                        "Amafrey, Betha, Cefrey, Kethra, Mara, Olga, Silifrey, Westra";

                                    nameList = humanIlluskanFemaleNames.Replace(" ", "").Split(',').ToList();

                                    return nameList;
                            }
                            break;

                        case "Mulan":
                            switch (sex)
                            {
                                case "Male":
                                    var humanMulanMaleNames =
                                        "Aoth, Bareris, Ehput-Ki, Kethoth, Mumed, Ramas, So-Kehur, Thazar-De, Urhur";

                                    nameList = humanMulanMaleNames.Replace(" ", "").Split(',').ToList();

                                    return nameList;
                                case "Female":
                                    var humanMulanFemaleNames =
                                        "Arizima, Chathi, Nephis, Nulara, Murithi, Sefris, Thola, Umara, Zolis";

                                    nameList = humanMulanFemaleNames.Replace(" ", "").Split(',').ToList();

                                    return nameList;
                            }
                            break;

                        case "Rashemi":
                            switch (sex)
                            {
                                case "Male":
                                    var humanRashemiMaleNames =
                                        "Borivik, Faurgar, Jandar, Kanithar, Madislak, Ralmevik, Shaumar, Vladislak";

                                    nameList = humanRashemiMaleNames.Replace(" ", "").Split(',').ToList();

                                    return nameList;
                                case "Female":
                                    var humanRashemiFemaleNames =
                                        "Fyevarra, Hulmarra, Immith, Imzel, Navarra, Shevarra, Tammith, Yuldra";

                                    nameList = humanRashemiFemaleNames.Replace(" ", "").Split(',').ToList();

                                    return nameList;
                            }
                            break;

                        case "Shou":
                            switch (sex)
                            {
                                case "Male":
                                    var humanShouMaleNames =
                                        "An, Chen, Chi, Fai, Jiang, Jun, Lian, Long, Meng, On, Shan, Shui, Wen";

                                    nameList = humanShouMaleNames.Replace(" ", "").Split(',').ToList();

                                    return nameList;
                                case "Female":
                                    var humanShouFemaleNames = "Bai, Chao, Jia, Lei, Mei, Qiao, Shui, Tai";

                                    nameList = humanShouFemaleNames.Replace(" ", "").Split(',').ToList();

                                    return nameList;
                            }
                            break;

                        case "Tethyrian":
                            switch (sex)
                            {
                                case "Male":
                                    var humanTethyrianMaleNames =
                                        "Darvin, Dorn, Evendur, Gorstag, Grim, Helm, Malark, Morn, Randal, Stedd";

                                    nameList = humanTethyrianMaleNames.Replace(" ", "").Split(',').ToList();

                                    return nameList;
                                case "Female":
                                    var humanTethyrianFemaleNames =
                                        "Arveene, Esvele, Jhessail, Kerri, Lureene, Miri, Rowan, Shandri, Tessele";

                                    nameList = humanTethyrianFemaleNames.Replace(" ", "").Split(',').ToList();

                                    return nameList;
                            }
                            break;

                        case "Turami":
                            switch (sex)
                            {
                                case "Male":
                                    var humanTuramiMaleNames =
                                        "Anton, Diero, Marcon, Pieron, Rimardo, Romero, Salazar, Umbero";

                                    nameList = humanTuramiMaleNames.Replace(" ", "").Split(',').ToList();

                                    return nameList;
                                case "Female":
                                    var humanTuramiFemaleNames =
                                        "Balama, Dona, Faila, Jalana, Luisa, Marta, Quara, Selise, Vonda";

                                    nameList = humanTuramiFemaleNames.Replace(" ", "").Split(',').ToList();

                                    return nameList;
                            }
                            break;
                    }
                    break;
            }
            return null;
        }

        public List<string> ClassListDeclare()
        {
            var classList = new List<string>
            {
                "Cleric",
                "Fighter",
                "Rogue",
                "Wizard"
            };

            return classList;
        }

        public List<string> AlignmentListDeclare()
        {
            var alignmentList = new List<string>
            {
                "Lawful good",
                "Neutral good",
                "Chaotic good",
                "Lawful neutral",
                "Neutral",
                "Chaotic neutral",
                "Lawful evil",
                "Neutral evil",
                "Chaotic evil"
            };
            return alignmentList;
        }

        public List<string> BackgroundListDeclare()
        {
            var bgList = new List<string>
            {
                "Acolyte",
                "Criminal",
                "Folk Hero",
                "Noble",
                "Sage",
                "Soldier"
            };
            return bgList;
        }

        public string GetBackgroundDescription(string background)
        {
            switch (background)
            {
                case "Acolyte":
                    return
                        "You have spent your life in the service of a temple \nto a specific god or pantheon of gods. You act as an \nintermediary between the realm of the holy and the \nmortal world, performing sacred rites and offering \nsacrifices in order to conduct worshipers into the \npresence of the divine.\n\nAcolytes are proficient in Insight and Religion.";
                case "Criminal":
                    return
                        "You are an experienced criminal with a history of \nbreaking the law. You have spent a lot of time among \nother criminals and still have contacts within the \ncriminal underworld. You’re far closer than most people \nto the world of murder, theft, and violence that pervades \nthe underbelly of civilization, and you have survived up to \nthis point by flouting the rules and regulations of society.\n\nCriminals are proficient in Deception and Stealth.";
                case "Folk Hero":
                    return
                        "You come from a humble social rank, but you are \ndestined for so much more. Already the people of \nyour home village regard you as their champion, and \nyour destiny calls you to stand against the tyrants and \nmonsters that threaten the common folk everywhere.\n\nFolk heroes are proficient in Animal Handling and Survival.";
                case "Noble":
                    return
                        "You understand wealth, power, and privilege. You carry \na noble title, and your family owns land, collects taxes, \nand wields significant political influence. You might be a \npampered aristocrat unfamiliar with work or discomfort, \na former merchant just elevated to the nobility, or a \ndisinherited scoundrel with a disproportionate sense of \nentitlement. Or you could be an honest, hard-working \nlandowner who cares deeply about the people who \nlive and work on your land, keenly aware of your \nresponsibility to them.\n\nNobles are proficient in History and Pursuasion.";
                case "Sage":
                    return
                        "You spent years learning the lore of the multiverse. You \nscoured manuscripts, studied scrolls, and listened to the \ngreatest experts on the subjects that interest you. Your \nefforts have made you a master in your fields of study.\n\nSages are proficient in Arcana and History.";
                case "Soldier":
                    return
                        "War has been your life for as long as you care to \nremember. You trained as a youth, studied the use of \nweapons and armor, learned basic survival techniques, \nincluding how to stay alive on the battlefield. You \nmight have been part of a standing national army or a \nmercenary company, or perhaps a member of a local \nmilitia who rose to prominence during a recent war.\n\nSoldiers are proficient in Athletics and Intimidation.";
                default:
                    return "Invalid background...";
            }
        }

        public Dictionary<string, List<string>> ProficiencyDictionaryDeclare()
        {
            var skills = new List<string>();
            var weapons = new List<string>();
            var armor = new List<string>();
            var tools = new List<string>();
            var proficiencyDictionary = new Dictionary<string, List<string>>
            {
                {"Skills", skills},
                {"Armor", armor},
                {"Weapons", weapons},
                {"Tools", tools}
            };

            return proficiencyDictionary;
        }

        public List<string> ClassSkillProficiencyDeclare(string playerClass)
        {
            switch (playerClass)
            {
                case "Cleric":
                    var clericSkills = "History, Insight, Medicine, Persuasion, Religion";
                    return clericSkills.Replace(" ", "").Split(',').ToList();
                case "Fighter":
                    var fighterSkills =
                        "Acrobatics, Animal Handling, Atheletics, History, Insight, Intimidaton, Perception, Survival";
                    return fighterSkills.Replace(" ", "").Split(',').ToList();
                case "Rogue":
                    var rogueSkills =
                        "Acrobatics, Athletics, Deception, Insight, Intimidation, Investigation, Perception, Performance, Persuasion, Sleight of Hand, Stealth";
                    return rogueSkills.Replace(" ", "").Split(',').ToList();
                case "Wizard":
                    var wizardSkills = "Arcana, History, Insight, Investigation, Medicine, Religion";
                    return wizardSkills.Replace(" ", "").Split(',').ToList();
            }
            return null;
        }

        public List<string> ClassOtherDelcare(string playerClass)
        {
            var other = new List<string>();
            switch (playerClass)
            {
                case "Cleric":
                    other.Add("Priests Pack");
                    other.Add("Holy Symbol");
                    return other;
                case "Fighter":
                    other.Add("Dungeoneers Pack");
                    return other;
                case "Rogue":
                    other.Add("Burglar's Pack");
                    return other;
                case "Wizard":
                    other.Add("Scholar's Pack");
                    other.Add("Spellbook");
                    return other;
            }
            return other;
        }

        public List<string> ClassToolsDeclare(string playerClass)
        {
            var tools = new List<string>();
            if (playerClass.Equals("Rogue"))
            {
                tools.Add("Theives' Tools");
            }
            return tools;
        }

        public List<string> ClassWeaponDeclare(string playerClass)
        {
            var weapon = new List<string>();
            switch (playerClass)
            {
                case "Cleric":
                    weapon.Add("Mace");
                    weapon.Add("Light Crossbow");
                    weapon.Add("20 Bolts");
                    return weapon;
                case "Fighter":
                    weapon.Add("Longsword");
                    weapon.Add("Light Crossbow");
                    weapon.Add("20 Bolts");
                    return weapon;
                case "Rogue":
                    weapon.Add("Rapier");
                    weapon.Add("Shortbow");
                    weapon.Add("20 Arrows");
                    weapon.Add("Dagger");
                    weapon.Add("Dagger");
                    return weapon;
                case "Wizard":
                    weapon.Add("Quatrestaff");
                    weapon.Add("Arcane Focus");
                    return weapon;
            }
            return null;
        }

        public List<string> ClassArmorDeclare(string playerClass)
        {
            var armor = new List<string>();
            switch (playerClass)
            {
                case "Cleric":
                    armor.Add("Scale Mail");
                    armor.Add("Shield");
                    return armor;
                case "Fighter":
                    armor.Add("Chain Mail");
                    armor.Add("Shield");
                    return armor;
                case "Rogue":
                    armor.Add("Leather Armor");
                    return armor;
                case "Wizard":
                    return armor;
            }
            return null;
        }

        public List<string> ClassEquipmentProficiencyDeclare(string playerClass)
        {
            switch (playerClass)
            {
                case "Cleric":
                    return
                        "Light Armor,Medium Armor,Shields,Simple Weapons"
                            .Split(',')
                            .ToList();
                case "Fighter":
                    return
                        "Light Armor,Medium Armor,Heavy Armor,Shields,Simple Weapons,Martial Weapons"
                            .Split(',')
                            .ToList();
                case "Rogue":
                    return
                        "Light Armor,Simple Weapons,Hand Crossbows,Longswords,Rapiers,Shortswords,Thieves' Tools"
                            .Split(',')
                            .ToList();
                case "Wizard":
                    return
                        "Daggers,Darts,Slings,Quatrestaffs,Light Crossbows"
                            .Split(',')
                            .ToList();
            }

            return null;
        }

        public Dictionary<string, List<string>> InventoryDictionaryDeclare()
        {
            var weapons = new List<string>();
            var armor = new List<string>();
            var tools = new List<string>();
            var other = new List<string>();
            var inventoryDictionary = new Dictionary<string, List<string>>
            {
                {"Armor", armor},
                {"Weapons", weapons},
                {"Tools", tools},
                {"Other", other}
            };

            return inventoryDictionary;
        }

        public List<string> SkillListDeclare()
        {
            return new List<string>
            {
                "Acrobatics",
                "Animal Handling",
                "Arcana",
                "Atheletics",
                "Deception",
                "History",
                "Insight",
                "Intimidation",
                "Investigation",
                "Medicine",
                "Nature",
                "Perception",
                "Performance",
                "Persuasion",
                "Religion",
                "Sleight of Hand",
                "Stealth",
                "Surivial"
            };
        }
    }
}