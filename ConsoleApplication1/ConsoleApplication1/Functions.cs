﻿namespace GameStudioAssignment
{
    public class Functions
    {
        public static void RaceStatBonuses(Character playerCharacter)
        {
            var race = playerCharacter.PlayerRace;
            var subRace = playerCharacter.PlayerSubRace;
            var stats = playerCharacter.PlayerStats;

            // Race Bonuses, Sub-Race Bonuses inside
            switch (race)
            {
                case "Dwarf":
                    stats["Constitution"] = stats["Constitution"] + 2;
                    switch (subRace)
                    {
                        case "Hill":
                            stats["Wisdom"]++;
                            break;
                        case "Mountain":
                            stats["Strength"] = stats["Strength"] + 2;
                            break;
                    }
                    break;

                case "Elf":
                    stats["Dexterity"] = stats["Dexterity"] + 2;
                    switch (subRace)
                    {
                        case "High":
                            stats["Intelligence"]++;
                            break;
                        case "Wood":
                            stats["Wisdom"]++;
                            break;
                    }
                    break;

                case "Halfling":
                    stats["Dexterity"] = stats["Dexterity"] + 2;
                    switch (subRace)
                    {
                        case "Lightfoot":
                            stats["Charisma"]++;
                            break;
                        case "Stout":
                            stats["Constitution"]++;
                            break;
                    }
                    break;

                case "Human":
                    stats["Strength"]++;
                    stats["Dexterity"]++;
                    stats["Constitution"]++;
                    stats["Intelligence"]++;
                    stats["Wisdom"]++;
                    stats["Charisma"]++;
                    break;
            }
        }

        public static void PopulateProficiencies(Character character)
        {
        }
    }
}