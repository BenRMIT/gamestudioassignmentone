﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace GameStudioAssignment
{
    public class Generators
    {
        static bool invalidinput;
        static bool redo;
        static string input;
        static Random rnd = new Random();


        public static string GenerateRace()
        {
            var raceList = new DictionaryAndListAssignment().RaceListDeclare();
            var raceNum = rnd.Next(0, raceList.Count);

            return raceList[raceNum];
        }

        public static string GenerateSubRace(string race)
        {
            var subRaceDictionary = new DictionaryAndListAssignment().SubRaceListDeclare(race);
            var subRaceNum = rnd.Next(0, subRaceDictionary.Count);

            return subRaceDictionary[subRaceNum];
        }

        public static string GenerateName(string race, string subrace, string sex)
        {
            var nameList = new DictionaryAndListAssignment().NameListDeclare(race, sex, subrace);
            var nameNum = rnd.Next(0, nameList.Count);

            return nameList[nameNum];
        }

        public static string GenerateSex()
        {
            var sexNum = rnd.Next(0, 2);

            switch (sexNum)
            {
                case 0:
                    return "Male";
                case 1:
                    return "Female";
            }
            return null;
        }

        public static string GenerateClass()
        {
            var classList = new DictionaryAndListAssignment().ClassListDeclare();
            var classNum = rnd.Next(0, classList.Count);

            return classList[classNum];
        }

        public static int GenerateStat()
        {
            var rollList = new List<int>();

            for (var i = 1; i <= 4; i++)
            {
                var currentRoll = rnd.Next(1, 7);
                Thread.Sleep(10);
                rollList.Add(currentRoll);
            }

            var smallest = 100;
            var total = 0;

            foreach (var roll in rollList)
            {
                if (roll < smallest)
                {
                    smallest = roll;
                }
            }

            rollList.Remove(smallest);

            foreach (var roll in rollList)
            {
                total = total + roll;
            }

            return total;
        }

        public static Dictionary<string, int> GenerateStatsDictionary()
        {
            Dictionary<string, int> stats = new DictionaryAndListAssignment().statDictionaryDeclare();
            stats["Strength"] = Generators.GenerateStat();
            stats["Dexterity"] = Generators.GenerateStat();
            stats["Constitution"] = Generators.GenerateStat();
            stats["Intelligence"] = Generators.GenerateStat();
            stats["Wisdom"] = Generators.GenerateStat();
            stats["Charisma"] = Generators.GenerateStat();

            return stats;
        }

        public static string GenerateAlignment()
        {
            var alignmentList = new DictionaryAndListAssignment().AlignmentListDeclare();
            var alignNum = rnd.Next(0, alignmentList.Count);

            return alignmentList[alignNum];
        }

        public static string GenerateBackground()
        {
            var bgList = new DictionaryAndListAssignment().BackgroundListDeclare();
            var bgNum = rnd.Next(0, bgList.Count);

            return bgList[bgNum];
        }

        public static void GenerateHitPoints(Character character)
        {
            var playerClass = character.PlayerClass;
            var modifier = character.PlayerStats["Constitution"];
            modifier = (modifier - 10) / 2;

            switch (playerClass)
            {
                case "Cleric":
                    character.PlayerHitPoints = 8 + modifier;
                    break;

                case "Fighter":
                    character.PlayerHitPoints = 10 + modifier;
                    break;

                case "Rogue":
                    character.PlayerHitPoints = 8 + modifier;
                    break;

                case "Wizard":
                    character.PlayerHitPoints = 6 + modifier;
                    break;
            }
        }

        public string[] ChooseAppearance()
        {
            string[] appearance = new string[3];

            do
            {
                redo = false;
                appearance[0] = ChooseRace();
                if (appearance[0].Equals("random"))
                {
                    appearance[0] = GenerateRace();
                    appearance[1] = GenerateSubRace(appearance[0]);
                    appearance[2] = GenerateSex();
                }
                else
                {
                    appearance[1] = ChooseSubRace(appearance[0]);

                    appearance[2] = ChooseSex();
                }

                do
                {
                    invalidinput = false;
                    Console.Clear();
                    Console.WriteLine("Choose your race!");
                    Console.WriteLine("---------------------------------------------");
                    Console.WriteLine("Your character will be a...");
                    Console.WriteLine();
                    Console.WriteLine(appearance[2] + " " + appearance[1] + " " + appearance[0] + ".");
                    Console.WriteLine();
                    Console.WriteLine("Is this okay?");
                    Console.WriteLine("1. Yes");
                    Console.WriteLine("2. No");
                    Console.WriteLine("r. Randomise");
                    Console.WriteLine("---------------------------------------------");
                    Console.Write(">>>");
                    input = Console.ReadLine();

                    switch (input)
                    {
                        case "1":
                            break;
                        case "2":
                            redo = true;
                            break;
                        case "r":
                            appearance[0] = GenerateRace();
                            appearance[1] = GenerateSubRace(appearance[0]);
                            appearance[2] = GenerateSex();
                            invalidinput = true;
                            break;
                        default:
                            Console.WriteLine("Invalid selection.");
                            Thread.Sleep(500);
                            invalidinput = true;
                            break;
                    }
                } while (invalidinput);
            } while (redo);

            return appearance;
        }

        public string ChooseRace()
        {
            string race = "";
            int choiceNumber;
            var raceList = new DictionaryAndListAssignment().RaceListDeclare();

            // Generates a menu with items from raceList, and a random option.
            do
            {
                invalidinput = false;
                Console.Clear();
                Console.WriteLine("Choose your race!");
                Console.WriteLine("---------------------------------------------");
                Console.WriteLine("Please choose a race for your character.");

                for (int i = 0; i < raceList.Count; i++)
                {
                    Console.WriteLine("{0}. {1}", i + 1, raceList[i]);
                }

                Console.WriteLine("r. Random");
                Console.WriteLine("---------------------------------------------");
                Console.Write(">>>");
                input = Console.ReadLine();

                // Reads the user's input and sets a race.
                if ((int.TryParse(input, out choiceNumber)) && (choiceNumber <= raceList.Count))
                {
                    race = raceList[choiceNumber - 1];
                }
                else if (input.Equals("r"))
                {
                    race = "random";
                }
                else
                {
                    Console.WriteLine("Invalid selection.");
                    Thread.Sleep(500);
                    invalidinput = true;
                }
            } while (invalidinput);
            return race;
        }

        public string ChooseSubRace(string race)
        {
            var subRaceList = new DictionaryAndListAssignment().SubRaceListDeclare(race);
            int choiceNumber;
            string subRace = "";

            // Generates a menu with items from subRaceList, and a random option.
            do
            {
                invalidinput = false;
                Console.Clear();
                Console.WriteLine("Choose your race!");
                Console.WriteLine("---------------------------------------------");
                Console.WriteLine("Choose a sub-race.");

                for (int i = 0; i < subRaceList.Count; i++)
                {
                    Console.WriteLine("{0}. {1} {2}", i + 1, subRaceList[i], race);
                }

                Console.WriteLine("r. Random");
                Console.WriteLine("---------------------------------------------");
                Console.Write(">>>");
                input = Console.ReadLine();

                // Reads the user's input and sets a sub-race.
                if ((int.TryParse(input, out choiceNumber)) && (choiceNumber <= subRaceList.Count))
                {
                    subRace = subRaceList[choiceNumber - 1];
                }
                else if (input.Equals("r"))
                {
                    subRace = GenerateSubRace(race);
                }
                else
                {
                    Console.WriteLine("Invalid selection.");
                    Thread.Sleep(500);
                    invalidinput = true;
                }
            } while (invalidinput);

            return subRace;
        }

        public string ChooseSex()
        {
            string sex = "";

            do
            {
                invalidinput = false;
                Console.Clear();
                Console.WriteLine("Choose your race!");
                Console.WriteLine("---------------------------------------------");
                Console.WriteLine("Choose a sex");
                Console.WriteLine("1. Male");
                Console.WriteLine("2. Female");
                Console.WriteLine("---------------------------------------------");
                Console.Write(">>>");
                input = Console.ReadLine();
                switch (input)
                {
                    case "1":
                        sex = "Male";
                        break;
                    case "2":
                        sex = "Female";
                        break;

                    default:
                        Console.WriteLine("Invalid selection.");
                        Thread.Sleep(500);
                        invalidinput = true;
                        break;
                }
            } while (invalidinput);
            return sex;
        }

        public string ChooseName(Character pc)
        {
            string name = "";

            do
            {
                redo = false;
                do
                {
                    invalidinput = false;
                    Console.Clear();
                    Console.WriteLine("Choose a name!");
                    Console.WriteLine("---------------------------------------------");
                    Console.WriteLine("How would you like to choose a name?");
                    Console.WriteLine("1. Input a custom name.");
                    Console.WriteLine("2. Generate a name for me!");
                    Console.WriteLine("---------------------------------------------");
                    Console.Write(">>>");
                    input = Console.ReadLine();
                    switch (input)
                    {
                        case "1":
                            Console.Clear();
                            Console.WriteLine("Choose a name!");
                            Console.WriteLine("---------------------------------------------");
                            Console.WriteLine("Please enter a name.");
                            Console.WriteLine("---------------------------------------------");
                            Console.Write(">>>");
                            input = Console.ReadLine();
                            // need to implement check for characters only
                            name = input;
                            break;
                        case "2":
                            name = GenerateName(pc.PlayerRace, pc.PlayerSubRace, pc.PlayerSex);
                            break;
                        default:
                            Console.WriteLine("Invalid selection.");
                            Thread.Sleep(500);
                            invalidinput = true;
                            break;
                    }
                } while (invalidinput);

                // Confirmation screen
                do
                {
                    invalidinput = false;
                    Console.Clear();
                    Console.WriteLine("Choose a name!");
                    Console.WriteLine("---------------------------------------------");
                    Console.WriteLine("Your character will be called " + name);
                    confirmationMenu();
                } while (invalidinput);
            } while (redo);
            return name;
        }

        public string ChooseClass()
        {
            string playerclass = "";
            int choiceNumber;
            var classList = new DictionaryAndListAssignment().ClassListDeclare();

            do
            {
                redo = false;

                // Generates a menu with items from classList, and a random option.
                do
                {
                    invalidinput = false;
                    Console.Clear();
                    Console.WriteLine("Choose a class!");
                    Console.WriteLine("---------------------------------------------");
                    Console.WriteLine("Please choose a character class.");

                    for (int i = 0; i < classList.Count; i++)
                    {
                        Console.WriteLine("{0}. {1}", i + 1, classList[i]);
                    }

                    Console.WriteLine("r. Random");
                    Console.WriteLine("---------------------------------------------");
                    Console.Write(">>>");
                    input = Console.ReadLine();

                    // Reads the user's input and sets a class.
                    if ((int.TryParse(input, out choiceNumber)) && (choiceNumber <= classList.Count))
                    {
                        playerclass = classList[choiceNumber - 1];
                    }
                    else if (input.Equals("r"))
                    {
                        playerclass = GenerateClass();
                        //Ask to confirm.
                        do
                        {
                            invalidinput = false;
                            Console.Clear();
                            Console.WriteLine("Choose a class!");
                            Console.WriteLine("---------------------------------------------");
                            Console.WriteLine("Your character's class will be: " + playerclass);
                            confirmationMenu();
                        } while (invalidinput);
                    }
                    else
                    {
                        Console.WriteLine("Invalid selection.");
                        Thread.Sleep(500);
                        invalidinput = true;
                    }
                } while (invalidinput);
            } while (redo);

            return playerclass;
        }

        public Dictionary<string, int> ChooseStats()
        {
            Dictionary<string, int> stats;
            bool statschanged = false;
            bool finished;
            string from = "";
            string to = "";

            do
            {
                redo = false;
                stats = GenerateStatsDictionary();

                do
                {
                    invalidinput = false;

                    Console.Clear();
                    Console.WriteLine("Decide on your ability scores!");
                    Console.WriteLine("---------------------------------------------");
                    Console.WriteLine("Your ability scores are...");

                    foreach (var ability in stats)
                    {
                        Console.WriteLine("{0, -14}{1} {2} {3}", ability.Key + ":", new String('*', ability.Value),
                            ability.Value, (ability.Value >= 18 ? "MAX" : ""));
                    }

                    Console.WriteLine();
                    Console.WriteLine("What would you like to do?");
                    Console.WriteLine("1. Accept");
                    if (!statschanged)
                    {
                        Console.WriteLine("2. Reallocate points");
                    }
                    Console.WriteLine("r. Reroll");
                    Console.WriteLine("---------------------------------------------");
                    Console.Write(">>>");
                    input = Console.ReadLine();
                    switch (input)
                    {
                        case "1":
                            break;
                        case "r":
                            redo = true;
                            statschanged = false;
                            break;
                        case "2":
                            finished = false;
                            //allow 6 changes to ability scores
                            for (int changes = 6; changes > 0; changes--)
                            {
                                do
                                {
                                    invalidinput = false;
                                    Console.Clear();
                                    Console.WriteLine("Decide on your ability scores!");
                                    Console.WriteLine("---------------------------------------------");
                                    Console.WriteLine("Reallocating ability scores...");
                                    foreach (var ability in stats)
                                    {
                                        Console.WriteLine("{0, -14}{1} {2} {3}", ability.Key + ":",
                                            new String('*', ability.Value), ability.Value,
                                            (ability.Value >= 18 ? "MAX" : ""));
                                    }
                                    Console.WriteLine();
                                    Console.WriteLine("Point reallocations remaining: {0}", changes);
                                    Console.WriteLine("Take a point from...");
                                    Console.WriteLine("1. Strength");
                                    Console.WriteLine("2. Dexterity");
                                    Console.WriteLine("3. Constitution");
                                    Console.WriteLine("4. Intelligence");
                                    Console.WriteLine("5. Wisdom");
                                    Console.WriteLine("6. Charisma");
                                    Console.WriteLine("7. Finish reallocating");
                                    Console.WriteLine("---------------------------------------------");
                                    Console.Write(">>>");
                                    input = Console.ReadLine();
                                    switch (input)
                                    {
                                        case "1":
                                            from = "Strength";
                                            break;
                                        case "2":
                                            from = "Dexterity";
                                            break;
                                        case "3":
                                            from = "Constitution";
                                            break;
                                        case "4":
                                            from = "Intelligence";
                                            break;
                                        case "5":
                                            from = "Wisdom";
                                            break;
                                        case "6":
                                            from = "Charisma";
                                            break;
                                        case "7":
                                            finished = true;
                                            changes = 0;
                                            break;
                                        default:
                                            Console.WriteLine("Invalid Selection.");
                                            Thread.Sleep(500);
                                            invalidinput = true;
                                            break;
                                    }
                                } while (invalidinput);

                                if (!finished)
                                {
                                    if (stats[from] > 0)
                                    {
                                        stats[from]--;
                                        do
                                        {
                                            invalidinput = false;
                                            Console.Clear();
                                            Console.WriteLine("Decide on your ability scores!");
                                            Console.WriteLine("---------------------------------------------");
                                            Console.WriteLine("Reallocating ability scores...");
                                            foreach (var ability in stats)
                                            {
                                                Console.WriteLine("{0, -14}{1} {2} {3}", ability.Key + ":",
                                                    new String('*', ability.Value), ability.Value,
                                                    (ability.Value >= 18 ? "MAX" : ""));
                                            }
                                            Console.WriteLine();
                                            Console.WriteLine("Point reallocations remaining: {0}", changes);
                                            Console.WriteLine("Add the point to...");
                                            Console.WriteLine("1. Strength");
                                            Console.WriteLine("2. Dexterity");
                                            Console.WriteLine("3. Constitution");
                                            Console.WriteLine("4. Intelligence");
                                            Console.WriteLine("5. Wisdom");
                                            Console.WriteLine("6. Charisma");
                                            Console.WriteLine("7. Cancel");
                                            Console.WriteLine("---------------------------------------------");
                                            Console.Write(">>>");
                                            input = Console.ReadLine();
                                            switch (input)
                                            {
                                                case "1":
                                                    to = "Strength";
                                                    break;
                                                case "2":
                                                    to = "Dexterity";
                                                    break;
                                                case "3":
                                                    to = "Constitution";
                                                    break;
                                                case "4":
                                                    to = "Intelligence";
                                                    break;
                                                case "5":
                                                    to = "Wisdom";
                                                    break;
                                                case "6":
                                                    to = "Charisma";
                                                    break;
                                                case "7":
                                                    to = from;
                                                    changes++;
                                                    break;
                                                default:
                                                    Console.WriteLine("Invalid Selection.");
                                                    Thread.Sleep(500);
                                                    invalidinput = true;
                                                    break;
                                            }
                                        } while (invalidinput);
                                        if (stats[to] < 18)
                                        {
                                            stats[to]++;
                                        }
                                        else
                                        {
                                            Console.WriteLine("Cannot add more to this score!");
                                            Console.WriteLine("Refunding point...");
                                            stats[from]++;
                                            Thread.Sleep(500);
                                            changes++;
                                        }
                                    }
                                    else
                                    {
                                        Console.WriteLine("You cannot reduce this stat any further!");
                                        Thread.Sleep(500);
                                        changes++;
                                    }
                                }
                            }
                            statschanged = true;
                            invalidinput = true; //return to the confirmation menu...
                            break;

                        default:
                            Console.WriteLine("Invalid selection.");
                            Thread.Sleep(500);
                            invalidinput = true;
                            break;
                    }
                } while (invalidinput);
            } while (redo);
            return stats;
        }

        public string ChooseAlignment()
        {
            int choiceNumber;
            string alignment = "";
            var alignmentList = new DictionaryAndListAssignment().AlignmentListDeclare();

            do
            {
                redo = false;

                // Generates a menu with items from aligntmentList, and a random option.
                do
                {
                    invalidinput = false;
                    Console.Clear();
                    Console.WriteLine("Choose an alignment!");
                    Console.WriteLine("---------------------------------------------");
                    Console.WriteLine("Please choose an alignment for your character.");

                    for (int i = 0; i < alignmentList.Count; i++)
                    {
                        Console.WriteLine("{0}. {1}", i + 1, alignmentList[i]);
                    }

                    Console.WriteLine("r. Random");
                    Console.WriteLine("---------------------------------------------");
                    Console.Write(">>>");
                    input = Console.ReadLine();

                    // Reads the user's input and sets an alignment.
                    if ((int.TryParse(input, out choiceNumber)) && (choiceNumber <= alignmentList.Count))
                    {
                        alignment = alignmentList[choiceNumber - 1];
                    }
                    else if (input.Equals("r"))
                    {
                        alignment = GenerateAlignment();
                        //Ask to confirm.
                        do
                        {
                            invalidinput = false;
                            Console.Clear();
                            Console.WriteLine("Choose an alignment!");
                            Console.WriteLine("---------------------------------------------");
                            Console.WriteLine("Your character's alignment is: " + alignment);
                            confirmationMenu();
                        } while (invalidinput);
                    }
                    else
                    {
                        Console.WriteLine("Invalid selection.");
                        Thread.Sleep(500);
                        invalidinput = true;
                    }
                } while (invalidinput);
            } while (redo);

            return alignment;
        }

        public string ChooseBackground()
        {
            int choiceNumber;
            var bg = "";
            var bgList = new DictionaryAndListAssignment().BackgroundListDeclare();

            do
            {
                redo = false;

                // Generates a menu with items from bgList, and a random option.
                do
                {
                    invalidinput = false;
                    Console.Clear();
                    Console.WriteLine("Choose a background!");
                    Console.WriteLine("---------------------------------------------");
                    Console.WriteLine("Please choose a background for your character.");

                    for (var i = 0; i < bgList.Count; i++)
                    {
                        Console.WriteLine("{0}. {1}", i + 1, bgList[i]);
                    }

                    Console.WriteLine("r. Random");
                    Console.WriteLine("---------------------------------------------");
                    Console.Write(">>>");
                    input = Console.ReadLine();

                    // Read the user's input and sets a background.
                    if ((int.TryParse(input, out choiceNumber)) && (choiceNumber <= bgList.Count))
                    {
                        bg = bgList[choiceNumber - 1];
                    }
                    else if (input.Equals("r"))
                    {
                        bg = GenerateBackground();
                    }
                    else
                    {
                        Console.WriteLine("Invalid selection.");
                        Thread.Sleep(500);
                        invalidinput = true;
                    }
                } while (invalidinput);

                //Ask to confirm.
                do
                {
                    invalidinput = false;
                    Console.Clear();
                    Console.WriteLine("Choose a background!");
                    Console.WriteLine("---------------------------------------------");
                    Console.WriteLine("You have chosen: " + bg);
                    Console.WriteLine();
                    Console.WriteLine(new DictionaryAndListAssignment().GetBackgroundDescription(bg));
                    confirmationMenu();
                } while (invalidinput);
            } while (redo);

            return bg;
        }

        public List<string> ChooseSkillProficiencies(Character pc)
        {
            string background = pc.PlayerBackground;
            string playerclass = pc.PlayerClass;
            string race = pc.PlayerRace;
            int choiceNumber;
            var classProfList = new DictionaryAndListAssignment().ClassSkillProficiencyDeclare(playerclass);
            var skillsList = new List<string>();
            int choicesRemaining;

            do
            {
                redo = false;
                skillsList.Clear();

                // Add race proficiencies to the skills list
                if (race.Equals("Elf"))
                {
                    skillsList.Add("Perception");
                }
                // Add background proficiencies to the skills list
                switch (background)
                {
                    case "Acolyte":
                        skillsList.Add("Insight");
                        skillsList.Add("Religion");
                        break;
                    case "Criminal":
                        skillsList.Add("Deception");
                        skillsList.Add("Stealth");
                        break;
                    case "Folk Hero":
                        skillsList.Add("Animal Handling");
                        skillsList.Add("Survival");
                        break;
                    case "Noble":
                        skillsList.Add("History");
                        skillsList.Add("Persuasion");
                        break;
                    case "Sage":
                        skillsList.Add("Arcana");
                        skillsList.Add("History");
                        break;
                    case "Soldier":
                        skillsList.Add("Athletics");
                        skillsList.Add("Intimidation");
                        break;
                }

                choicesRemaining = playerclass.Equals("Rogue") ? 4 : 2;
                while (choicesRemaining > 0)
                {
                    do
                    {
                        invalidinput = false;
                        Console.Clear();
                        Console.WriteLine("Choose your skills!");
                        Console.WriteLine("---------------------------------------------");
                        Console.WriteLine("Which skill do you want to add?");
                        Console.WriteLine();
                        Console.WriteLine("Your class ({0}) allows you to choose from these skills:", playerclass);
                        Console.WriteLine("Choices remaining: " + choicesRemaining);

                        for (int i = 0; i < classProfList.Count; i++)
                        {
                            Console.WriteLine("{0}. {1}", i + 1, classProfList[i]);
                        }
                        Console.WriteLine("r. Random");

                        Console.WriteLine();
                        Console.WriteLine("Learnt skills:");
                        foreach (var skill in skillsList)
                        {
                            Console.WriteLine(skill);
                        }

                        Console.WriteLine();
                        Console.WriteLine("---------------------------------------------");
                        Console.Write(">>>");
                        input = Console.ReadLine();

                        // Reads user's input and adds the chosen skill to a list
                        if ((int.TryParse(input, out choiceNumber)) && (choiceNumber <= classProfList.Count))
                        {
                            if (!skillsList.Contains(classProfList[choiceNumber - 1]))
                            {
                                skillsList.Add(classProfList[choiceNumber - 1]);
                                choicesRemaining--;
                            }
                            else
                            {
                                Console.WriteLine("You already know that skill!");
                                Thread.Sleep(500);
                                invalidinput = true;
                            }
                        }
                        else if (input.Equals("r"))
                        {
                            do
                            {
                                choiceNumber = rnd.Next(0, classProfList.Count);
                            } while (skillsList.Contains(classProfList[choiceNumber]));
                            skillsList.Add(classProfList[choiceNumber]);
                            choicesRemaining--;
                        }
                        else
                        {
                            Console.WriteLine("Invalid selection.");
                            Thread.Sleep(500);
                            invalidinput = true;
                        }
                    } while (invalidinput);
                }
                // Ask for confirmation
                do
                {
                    invalidinput = false;
                    Console.Clear();
                    Console.WriteLine("Choose your skills!");
                    Console.WriteLine("---------------------------------------------");
                    Console.WriteLine("Your chosen skills are: ");
                    foreach (var skill in skillsList)
                    {
                        Console.WriteLine(skill);
                    }
                    confirmationMenu();
                } while (invalidinput);
            } while (redo);

            return skillsList;
        }

        private void confirmationMenu()
        {
            Console.WriteLine();
            Console.WriteLine("Is this okay?");
            Console.WriteLine("1. Yes");
            Console.WriteLine("2. No");
            Console.WriteLine("---------------------------------------------");
            Console.Write(">>>");
            input = Console.ReadLine();
            switch (input)
            {
                case "1":
                    break;
                case "2":
                    redo = true;
                    break;
                default:
                    Console.WriteLine("Invalid selection.");
                    Thread.Sleep(500);
                    invalidinput = true;
                    break;
            }
        }

        public static List<string> GenerateSkillProficiencies(string playerClass, string background)
        {
            var classSkillProfDictionary = new DictionaryAndListAssignment().ClassSkillProficiencyDeclare(playerClass);
            var skillListFull = new DictionaryAndListAssignment().SkillListDeclare();
            var skillList = new List<string>();

            if (playerClass.Equals("Rogue"))
            {
                for (var i = 0; i < 4; i++)
                {
                    var profNum = rnd.Next(0, classSkillProfDictionary.Count);
                    skillList.Add(classSkillProfDictionary[profNum]);
                }
            }
            else
            {
                for (var i = 0; i < 2; i++)
                {
                    var profNum = rnd.Next(0, classSkillProfDictionary.Count);
                    skillList.Add(classSkillProfDictionary[profNum]);
                }
            }


            bool history;
            var randomNum = 0;
            switch (background)
            {
                case "Acolyte":
                    var insight = true;
                    var religion = true;
                    foreach (var skill in skillList)
                    {
                        if (skill.Equals("Insight"))
                        {
                            insight = false;
                        }
                        else if (skill.Equals("Religion"))
                        {
                            religion = false;
                        }
                    }

                    if (insight)
                    {
                        skillList.Add("Insight");
                    }
                    else
                    {
                        skillListFull.Remove("Insight");
                        randomNum++;
                    }

                    if (religion)
                    {
                        skillList.Add("Religion");
                    }
                    else
                    {
                        skillListFull.Remove("Religion");
                        randomNum++;
                    }

                    for (var i = 0; i < randomNum; i++)
                    {
                        var toAdd = rnd.Next(0, skillListFull.Count + 1);
                        skillList.Add(skillListFull[toAdd]);
                        skillListFull.Remove(skillListFull[toAdd]);
                    }
                    break;
                case "Criminal":
                    var deception = true;
                    var stealth = true;
                    foreach (var skill in skillList)
                    {
                        if (skill.Equals("Deception"))
                        {
                            deception = false;
                        }
                        else if (skill.Equals("Stealth"))
                        {
                            stealth = false;
                        }
                    }

                    if (deception)
                    {
                        skillList.Add("Deception");
                    }
                    else
                    {
                        skillListFull.Remove("Deception");
                        randomNum++;
                    }

                    if (stealth)
                    {
                        skillList.Add("Stealth");
                    }
                    else
                    {
                        skillListFull.Remove("Stealth");
                        randomNum++;
                    }
                    for (var i = 0; i < randomNum; i++)
                    {
                        var toAdd = rnd.Next(0, skillListFull.Count + 1);
                        skillList.Add(skillListFull[toAdd]);
                        skillListFull.Remove(skillListFull[toAdd]);
                    }
                    break;
                case "Folk Hero":
                    var animalHandling = true;
                    var survival = true;
                    foreach (var skill in skillList)
                    {
                        if (skill.Equals("Animal Handling"))
                        {
                            animalHandling = false;
                        }
                        else if (skill.Equals("Survival"))
                        {
                            survival = false;
                        }
                    }

                    if (animalHandling)
                    {
                        skillList.Add("Animal Handling");
                    }
                    else
                    {
                        skillListFull.Remove("Animal Handling");
                        randomNum++;
                    }

                    if (survival)
                    {
                        skillList.Add("Survival");
                    }
                    else
                    {
                        skillListFull.Remove("Survival");
                        randomNum++;
                    }
                    for (var i = 0; i < randomNum; i++)
                    {
                        var toAdd = rnd.Next(0, skillListFull.Count + 1);
                        skillList.Add(skillListFull[toAdd]);
                        skillListFull.Remove(skillListFull[toAdd]);
                    }
                    break;
                case "Noble":
                    history = true;
                    var persuasion = true;
                    foreach (var skill in skillList)
                    {
                        if (skill.Equals("History"))
                        {
                            history = false;
                        }
                        else if (skill.Equals("Persuasion"))
                        {
                            persuasion = false;
                        }
                    }

                    if (history)
                    {
                        skillList.Add("History");
                    }
                    else
                    {
                        skillListFull.Remove("History");
                        randomNum++;
                    }

                    if (persuasion)
                    {
                        skillList.Add("Persuasion");
                    }
                    else
                    {
                        skillListFull.Remove("Persuasion");
                        randomNum++;
                    }
                    for (var i = 0; i < randomNum; i++)
                    {
                        var toAdd = rnd.Next(0, skillListFull.Count + 1);
                        skillList.Add(skillListFull[toAdd]);
                        skillListFull.Remove(skillListFull[toAdd]);
                    }
                    break;
                case "Sage":
                    var arcana = true;
                    history = true;

                    foreach (var skill in skillList)
                    {
                        if (skill.Equals("Arcana"))
                        {
                            arcana = false;
                        }
                        else if (skill.Equals("History"))
                        {
                            history = false;
                        }
                    }

                    if (arcana)
                    {
                        skillList.Add("Arcana");
                    }
                    else
                    {
                        skillListFull.Remove("Arcana");
                        randomNum++;
                    }

                    if (history)
                    {
                        skillList.Add("History");
                    }
                    else
                    {
                        skillListFull.Remove("History");
                        randomNum++;
                    }
                    for (var i = 0; i < randomNum; i++)
                    {
                        var toAdd = rnd.Next(0, skillListFull.Count + 1);
                        skillList.Add(skillListFull[toAdd]);
                        skillListFull.Remove(skillListFull[toAdd]);
                    }
                    break;
                case "Soldier":
                    var athletics = true;
                    var intimidation = true;

                    foreach (var skill in skillList)
                    {
                        if (skill.Equals("Athletics"))
                        {
                            athletics = false;
                        }
                        else if (skill.Equals("Intimidation"))
                        {
                            intimidation = false;
                        }
                    }

                    if (athletics)
                    {
                        skillList.Add("Athletics");
                    }
                    else
                    {
                        skillListFull.Remove("Athletics");
                        randomNum++;
                    }

                    if (intimidation)
                    {
                        skillList.Add("Intimidation");
                    }
                    else
                    {
                        skillListFull.Remove("Intimidation");
                        randomNum++;
                    }
                    for (var i = 0; i < randomNum; i++)
                    {
                        var toAdd = rnd.Next(0, skillListFull.Count + 1);
                        skillList.Add(skillListFull[toAdd]);
                        skillListFull.Remove(skillListFull[toAdd]);
                    }
                    break;
            }

            return skillList;
        }

        public static List<string> GenerateSavingThrows(string playerClass)
        {
            switch (playerClass)
            {
                case "Cleric":
                    return "Wisdom, Charisma".Replace(" ", "").Split(',').ToList();
                case "Fighter":
                    return "Strength, Constitution".Replace(" ", "").Split(',').ToList();
                case "Rogue":
                    return "Dexterity, Intelligence".Replace(" ", "").Split(',').ToList();
                case "Wizard":
                    return "Intelligence, Wisdom".Replace(" ", "").Split(',').ToList();
            }
            return null;
        }

        public static void GenerateEquipment(Dictionary<string, List<string>> playerInventory, string playerClass)
        {
            playerInventory["Armor"] = new DictionaryAndListAssignment().ClassArmorDeclare(playerClass);
            playerInventory["Weapons"] = new DictionaryAndListAssignment().ClassWeaponDeclare(playerClass);
            playerInventory["Tools"] = new DictionaryAndListAssignment().ClassToolsDeclare(playerClass);
            playerInventory["Other"] = new DictionaryAndListAssignment().ClassOtherDelcare(playerClass);
        }

        public static void setLevel(Character pc)
        {
            if ((pc.PlayerExp >= 0) && (pc.PlayerExp < 300))
            {
                pc.PlayerLevel = 1;
            }
            else if ((pc.PlayerExp >= 300) && (pc.PlayerExp < 900))
            {
                pc.PlayerLevel = 2;
            }
            else if ((pc.PlayerExp >= 900) && (pc.PlayerExp < 2700))
            {
                pc.PlayerLevel = 3;
            }
            else if ((pc.PlayerExp >= 2700) && (pc.PlayerExp < 6500))
            {
                pc.PlayerLevel = 4;
            }
            else if ((pc.PlayerExp >= 6500) && (pc.PlayerExp < 14000))
            {
                pc.PlayerLevel = 5;
            }
            else if ((pc.PlayerExp >= 14000) && (pc.PlayerExp < 23000))
            {
                pc.PlayerLevel = 6;
            }
            else if ((pc.PlayerExp >= 23000) && (pc.PlayerExp < 34000))
            {
                pc.PlayerLevel = 7;
            }
            else if ((pc.PlayerExp >= 34000) && (pc.PlayerExp < 48000))
            {
                pc.PlayerLevel = 8;
            }
            else if ((pc.PlayerExp >= 48000) && (pc.PlayerExp < 64000))
            {
                pc.PlayerLevel = 9;
            }
            else if ((pc.PlayerExp >= 64000) && (pc.PlayerExp < 85000))
            {
                pc.PlayerLevel = 10;
            }
            else if ((pc.PlayerExp >= 85000) && (pc.PlayerExp < 100000))
            {
                pc.PlayerLevel = 11;
            }
            else if ((pc.PlayerExp >= 100000) && (pc.PlayerExp < 120000))
            {
                pc.PlayerLevel = 12;
            }
            else if ((pc.PlayerExp >= 120000) && (pc.PlayerExp < 140000))
            {
                pc.PlayerLevel = 13;
            }
            else if ((pc.PlayerExp >= 140000) && (pc.PlayerExp < 165000))
            {
                pc.PlayerLevel = 14;
            }
            else if ((pc.PlayerExp >= 165000) && (pc.PlayerExp < 195000))
            {
                pc.PlayerLevel = 15;
            }
            else if ((pc.PlayerExp >= 195000) && (pc.PlayerExp < 225000))
            {
                pc.PlayerLevel = 16;
            }
            else if ((pc.PlayerExp >= 225000) && (pc.PlayerExp < 265000))
            {
                pc.PlayerLevel = 17;
            }
            else if ((pc.PlayerExp >= 265000) && (pc.PlayerExp < 305000))
            {
                pc.PlayerLevel = 18;
            }
            else if ((pc.PlayerExp >= 305000) && (pc.PlayerExp < 355000))
            {
                pc.PlayerLevel = 19;
            }
            else if (pc.PlayerExp >= 355000)
            {
                pc.PlayerLevel = 20;
            }
            else
            {
                pc.PlayerLevel = 1;
            }
        }

        public static void setSpeed(Character pc)
        {
            if (pc.PlayerRace.Equals("Dwarf") || pc.PlayerRace.Equals("Halfling"))
            {
                pc.PlayerSpeed = 25;
            }
            else if (pc.PlayerFullRace.Equals("Woodland Elf"))
            {
                pc.PlayerSpeed = 35;
            }
            else
            {
                pc.PlayerSpeed = 30;
            }
        }

        public static void setArmorClass(Character pc)
        {
            int armorClass = 10;
            foreach (var item in pc.PlayerInventory["Armor"])
            {
                switch (item)
                {
                    case "Padded Armor":
                        armorClass = 11 + toModifier(pc.PlayerStats["Dexterity"]);
                        break;
                    case "Leather Armor":
                        armorClass = 11 + toModifier(pc.PlayerStats["Dexterity"]);
                        break;
                    case "Studded Armor":
                        armorClass = 12 + toModifier(pc.PlayerStats["Dexterity"]);
                        break;
                    case "Hide Armor":
                        armorClass = 12 + toModifier(pc.PlayerStats["Dexterity"]);
                        break;
                    case "Chain Shirt":
                        armorClass = 13 + toModifier(pc.PlayerStats["Dexterity"]);
                        break;
                    case "Scale Mail":
                        armorClass = 14 + toModifier(pc.PlayerStats["Dexterity"]);
                        break;
                    case "Breastplate":
                        armorClass = 14 + toModifier(pc.PlayerStats["Dexterity"]);
                        break;
                    case "Half Plate":
                        armorClass = 15 + toModifier(pc.PlayerStats["Dexterity"]);
                        break;
                    case "Ring Mail":
                        armorClass = 14;
                        break;
                    case "Chain Mail":
                        armorClass = 16;
                        if (pc.PlayerStats["Strength"] < 13)
                        {
                            pc.PlayerSpeed -= 10;
                        }
                        break;
                    case "Splint Armor":
                        armorClass = 17;
                        if (pc.PlayerStats["Strength"] < 15)
                        {
                            pc.PlayerSpeed -= 10;
                        }
                        break;
                    case "Plate Armor":
                        armorClass = 18;
                        if (pc.PlayerStats["Strength"] < 15)
                        {
                            pc.PlayerSpeed -= 10;
                        }
                        break;
                    case "Shield":
                        armorClass += 2;
                        break;
                    default:
                        break;
                }
                pc.PlayerArmorClass = armorClass;
            }
        }

        public static int toModifier(int score)
        {
            return (score - 10) / 2;
        }
    }
}