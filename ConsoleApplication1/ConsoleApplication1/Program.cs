﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;

namespace GameStudioAssignment
{
    [Serializable]
    public class Character
    {
        public Character()
        {
        }

        public string PlayerName { get; set; }

        public string PlayerSex { get; set; }

        public string PlayerClass { get; set; }

        public string PlayerRace { get; set; }

        public string PlayerSubRace { get; set; }

        public string PlayerFullRace { get; set; }

        public string PlayerAlignment { get; set; }

        public string PlayerBackground { get; set; }

        public int PlayerHitPoints { get; set; }

        public int PlayerExp { get; set; }

        public int PlayerLevel { get; set; }

        public int PlayerSpeed { get; set; }

        public int PlayerArmorClass { get; set; }

        public Dictionary<string, int> PlayerStats { get; set; }

        public Dictionary<string, List<string>> PlayerProficiencies { get; set; }

        public List<string> PlayerSavingThrows { get; set; }

        public Dictionary<string, List<string>> PlayerInventory { get; set; }

        public int PlayerGold { get; set; }
    }

    internal static class Program
    {
        public static void Main(string[] args)
        {
            MainMenu();
        }

        public static void MainMenu()
        {
            while (true)
            {
                Console.Clear();
                Console.WriteLine("Welcome to Team Drider's Character Generator!");
                Console.WriteLine("---------------------------------------------");
                Thread.Sleep(500);
                Console.WriteLine("What would you like to do?");
                Console.WriteLine("1. Generate a Random Character");
                Console.WriteLine("2. Manually create a character");
                Console.WriteLine("3. Load a character");
                Console.WriteLine("4. Help");
                Console.WriteLine("5. Quit");
                Console.WriteLine("---------------------------------------------");

                Console.Write(">>>");
                var userChoice = Console.ReadLine();

                if (userChoice.Equals("1"))
                {
                    RandomGenerateMenu();
                }
                else if (userChoice.Equals("2"))
                {
                    ManualGenerate();
                }
                else if (userChoice.Equals("3"))
                {
                    LoadMenu();
                }
                else if (userChoice.Equals("4"))
                {
                    HelpMenu();
                }
                else if (userChoice.Equals("5"))
                {
                    Environment.Exit(1);
                }
                else
                {
                    Console.WriteLine("ERROR: Please input a valid value.");
                    Thread.Sleep(2000);
                }
            }
        }

        public static void RandomGenerateMenu()
        {
            while (true)
            {
                Console.Clear();
                Console.WriteLine("Welcome to the Random Generator!");
                Thread.Sleep(500);
                Console.WriteLine("---------------------------------------------");
                Console.WriteLine("What would you like to do?");
                Console.WriteLine("1. Begin");
                Console.WriteLine("2. Back");
                Console.WriteLine("---------------------------------------------");

                Console.Write(">>>");
                var userChoice = Console.ReadLine();

                if (userChoice.Equals("1"))
                {
                    RandomGenerate();
                }
                else if (userChoice.Equals("2"))
                {
                    Console.WriteLine("Returning to Main Menu...");
                    Thread.Sleep(1000);
                }
                else
                {
                    Console.WriteLine("ERROR: Please input a valid value");
                    Thread.Sleep(2000);
                    continue;
                }
                break;
            }
        }

        public static void RandomGenerate()
        {
            // Declare playerCharacter object and construct initial race
            var playerCharacter = new Character
            {
                PlayerRace = Generators.GenerateRace(),
                PlayerProficiencies = new DictionaryAndListAssignment().ProficiencyDictionaryDeclare(),
                PlayerInventory = new DictionaryAndListAssignment().InventoryDictionaryDeclare(),
                PlayerExp = 0
            };

            // Construct the Sub Race and thus the Full Race Name
            playerCharacter.PlayerSubRace = Generators.GenerateSubRace(playerCharacter.PlayerRace);
            playerCharacter.PlayerFullRace = playerCharacter.PlayerSubRace + " " + playerCharacter.PlayerRace;

            // Construct the Player Sex
            playerCharacter.PlayerSex = Generators.GenerateSex();

            // Construct the Player Name
            playerCharacter.PlayerName = Generators.GenerateName(playerCharacter.PlayerRace,
                playerCharacter.PlayerSubRace, playerCharacter.PlayerSex);

            // Construct the Player Class
            playerCharacter.PlayerClass = Generators.GenerateClass();

            // Construct the Player Stats
            playerCharacter.PlayerStats = Generators.GenerateStatsDictionary();

            // Construct alignment
            playerCharacter.PlayerAlignment = Generators.GenerateAlignment();

            // Construct character's background
            playerCharacter.PlayerBackground = Generators.GenerateBackground();

            // Add Class and Race bonuses to stats
            Functions.RaceStatBonuses(playerCharacter);

            // Generate Hit points
            Generators.GenerateHitPoints(playerCharacter);

            // Generate Skill Proficiencies
            playerCharacter.PlayerProficiencies["Skills"] =
                Generators.GenerateSkillProficiencies(playerCharacter.PlayerClass, playerCharacter.PlayerBackground);

            // Generate Saving Throws
            playerCharacter.PlayerSavingThrows = Generators.GenerateSavingThrows(playerCharacter.PlayerClass);

            // Generate Equipment from Class
            Generators.GenerateEquipment(playerCharacter.PlayerInventory, playerCharacter.PlayerClass);

            // Sets speed attribute
            Generators.setSpeed(playerCharacter);

            // Sets Armor class and speed modifiers based on armor in inventory
            Generators.setArmorClass(playerCharacter);

            // Set the character's and level
            Generators.setLevel(playerCharacter);

            CharacterSheet.CharacterSheetMenu(playerCharacter);
        }

        public static void ManualGenerate()
        {
            var playerCharacter = new Character
            {
                PlayerExp = 0,
                PlayerProficiencies = new DictionaryAndListAssignment().ProficiencyDictionaryDeclare(),
                PlayerInventory = new DictionaryAndListAssignment().InventoryDictionaryDeclare()
            };
            var generator = new Generators();

            Console.Clear();
            Console.WriteLine("Welcome to the Manual Character Creator!");
            Thread.Sleep(1000);

            //Construct the Player Race, SubRace and Sex from user input
            var appearance = generator.ChooseAppearance();
            playerCharacter.PlayerRace = appearance[0];
            playerCharacter.PlayerSubRace = appearance[1];
            playerCharacter.PlayerFullRace = playerCharacter.PlayerSubRace + " " + playerCharacter.PlayerRace;
            playerCharacter.PlayerSex = appearance[2];

            //Construct the Player Name from user input
            playerCharacter.PlayerName = generator.ChooseName(playerCharacter);

            //Construct the Player Class
            playerCharacter.PlayerClass = generator.ChooseClass();

            //Construct character background
            playerCharacter.PlayerBackground = generator.ChooseBackground();

            //Generates skills based on class/race/background
            playerCharacter.PlayerProficiencies["Skills"] = generator.ChooseSkillProficiencies(playerCharacter);

            //Construct alignment
            playerCharacter.PlayerAlignment = generator.ChooseAlignment();

            //Construct the Player Stats
            playerCharacter.PlayerStats = generator.ChooseStats();

            // Add Class and Race bonuses to stats
            Functions.RaceStatBonuses(playerCharacter);

            // Generate Hit points
            Generators.GenerateHitPoints(playerCharacter);

            // Generate Saving Throws
            playerCharacter.PlayerSavingThrows = Generators.GenerateSavingThrows(playerCharacter.PlayerClass);

            // Sets speed attribute
            Generators.setSpeed(playerCharacter);

            // Generate Equipment from Class
            Generators.GenerateEquipment(playerCharacter.PlayerInventory, playerCharacter.PlayerClass);

            // Sets Armor class and speed modifiers based on armor in inventory
            Generators.setArmorClass(playerCharacter);

            // Sets the character's level
            Generators.setLevel(playerCharacter);

            CharacterSheet.CharacterSheetMenu(playerCharacter);
        }


        public static void LoadMenu()
        {
            var listNames = new List<string>();

            listNames = File.ReadAllText("Saves\\SaveList.txt").Split(',').ToList();
            var validation = true;

            do
            {

                var num = 1;
                foreach (var name in listNames)
                {
                    Console.WriteLine(num + ". " + name);
                    num++;
                }

                Console.WriteLine("Please pick a save: ");
                var saveNum = Console.ReadLine();
                var saveNumInt = int.Parse(saveNum);


                if (saveNumInt <= listNames.Count && saveNumInt > 0)
                {
                    validation = false;
                    saveNumInt = saveNumInt - 1;
                    var character = DeserializeFromString<Character>(listNames[saveNumInt]);
                    Console.WriteLine("Loading " + listNames[saveNumInt] + "...");
                    Load(character);
                }
                else
                {
                    Console.WriteLine("Invalid Input");
                    Thread.Sleep(500);
                }
            } while (validation);
        }

        private static void Load(Character character)
        {
            CharacterSheet.CharacterSheetMenu(character);
        }

        private static string readFile(string fileName)
        {
            return File.ReadAllText(Path.Combine(Environment.CurrentDirectory, "Saves/" + fileName + ".txt"));
        }

        private static TData DeserializeFromString<TData>(string fileName)
        {
            var settings = readFile(fileName);

            var b = Convert.FromBase64String(settings);
            using (var stream = new MemoryStream(b))
            {
                var formatter = new BinaryFormatter();
                stream.Seek(0, SeekOrigin.Begin);
                return (TData) formatter.Deserialize(stream);
            }
        }

        public static void HelpMenu()
        {
            Console.WriteLine("Coming soon");
            Thread.Sleep(1000);
        }
    }
}