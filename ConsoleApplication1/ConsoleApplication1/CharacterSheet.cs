﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;

namespace GameStudioAssignment
{
    public class CharacterSheet
    {
        public static void CharacterSheetMenu(Character character)
        {
            while (true)
            {
                Console.Clear();
                Console.WriteLine("Welcome to " + character.PlayerName + "'s Character Sheet!");
                Thread.Sleep(1000);
                Console.WriteLine("What would you like to do?");
                Console.WriteLine("-------––------------------------------------");
                Console.WriteLine("1. Basic Info");
                Console.WriteLine("2. Inventory");
                Console.WriteLine("3. Save Character");
                Console.WriteLine("4. Return to Main Menu");
                Console.WriteLine("5. Exit");

                Console.Write(">>>");
                var userChoice = Console.ReadLine();

                if (userChoice.Equals("1"))
                {
                    DisplayBasicInfo(character);
                }
                else if (userChoice.Equals("2"))
                {
                    DisplayInventory(character);
                }
                else if (userChoice.Equals("3"))
                {
                    Save(character);
                }
                else if (userChoice.Equals("4"))
                {
                }
                else if (userChoice.Equals("5"))
                {
                    Environment.Exit(2);
                }
                else
                {
                    Console.WriteLine("ERROR: Please input a valid value.");
                    Thread.Sleep(2000);
                    continue;
                }
                break;
            }
        }

        private static void DisplayBasicInfo(Character character)
        {
            Console.Clear();
            Console.WriteLine("-----------------Basic Info----------------------");
            Console.WriteLine("Name: " + character.PlayerName);
            Console.WriteLine("Sex: " + character.PlayerSex);
            Console.WriteLine("Race: " + character.PlayerFullRace);
            Console.WriteLine("    Main Race: " + character.PlayerRace);
            Console.WriteLine("    Sub Race: " + character.PlayerSubRace);
            Console.WriteLine("Class: " + character.PlayerClass);
            Console.WriteLine("Hit Points: " + character.PlayerHitPoints);
            Console.WriteLine("Armor Class: " + character.PlayerArmorClass);
            Console.WriteLine("--------------------Stats------------------------");
            Console.WriteLine("Strength : " + character.PlayerStats["Strength"]);
            Console.WriteLine("Dexterity : " + character.PlayerStats["Dexterity"]);
            Console.WriteLine("Constitution : " + character.PlayerStats["Constitution"]);
            Console.WriteLine("Intelligence : " + character.PlayerStats["Intelligence"]);
            Console.WriteLine("Wisdom : " + character.PlayerStats["Wisdom"]);
            Console.WriteLine("Charisma : " + character.PlayerStats["Charisma"]);
            Console.WriteLine("Speed : " + character.PlayerSpeed);
            Console.WriteLine("--------------------Skills------------------------");
            foreach (var skill in character.PlayerProficiencies["Skills"])
            {
                Console.WriteLine(skill);
            }
            Console.WriteLine("----------------------------------------------------");


            Console.Write("Press enter to return...");
            Console.ReadLine();
            CharacterSheetMenu(character);
        }

        private static void DisplayInventory(Character character)
        {
            Console.WriteLine("-------------------Inventory----------------------");
            Console.WriteLine();

            Console.WriteLine("Armor :");
            foreach (var item in character.PlayerInventory["Armor"])
            {
                Console.WriteLine("    " + item);
            }
            Console.WriteLine("Weapons :");
            foreach (var item in character.PlayerInventory["Weapons"])
            {
                Console.WriteLine("    " + item);
            }
            Console.WriteLine("Tools :");
            foreach (var item in character.PlayerInventory["Tools"])
            {
                Console.WriteLine("    " + item);
            }
            Console.WriteLine("Misc :");
            foreach (var item in character.PlayerInventory["Other"])
            {
                Console.WriteLine("    " + item);
            }

            Thread.Sleep(1000);
            Console.WriteLine("----------------------------------------------------");
            CharacterSheetMenu(character);
        }

        private static void Save(Character character)
        {
            SaveList(character);
            SaveCharacter(character);
        }

        private static void SaveList(Character character)
        {
            File.AppendAllText("Saves\\SaveList.txt", "," + character.PlayerName);
        }

        private static void SaveCharacter(Character character)
        {
            File.WriteAllText("Saves\\" + character.PlayerName.Replace(" ", "") + ".txt", SerializeToString(character));
        }

        private static string SerializeToString<Character>(Character character)
        {
            using (var stream = new MemoryStream())
            {
                var formatter = new BinaryFormatter();
                formatter.Serialize(stream, character);
                stream.Flush();
                stream.Position = 0;
                return Convert.ToBase64String(stream.ToArray());
            }
        }
    }
}